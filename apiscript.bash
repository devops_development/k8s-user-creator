# Export the internal Kubernetes API server hostname
APISERVER=https://192.168.1.164:6443

# Export the path to ServiceAccount mount directory
# SERVICEACCOUNT=/home/juno/.kube/users

# Read the ServiceAccount bearer token
# TOKEN=$(cat ${SERVICEACCOUNT}/client-ca.key)
TOKEN=$(kubectl get secret default-token -o jsonpath='{.data.token}' | base64 --decode)


# Reference the internal Kubernetes certificate authority (CA)
# CACERT=${SERVICEACCOUNT}/client-ca.crt

# Make a call to the Kubernetes API with TOKEN
# curl --cacert ${CACERT} --header "Authorization: Bearer ${TOKEN}" -X GET ${APISERVER}/api/v1/namespaces/default/pods --insecure

# curl --header "Authorization: Bearer ${TOKEN}" -X GET ${APISERVER}/api/v1/namespaces/default/pods --insecure

curl --header "Authorization: Bearer ${TOKEN}" -X GET ${APISERVER}/api --insecure