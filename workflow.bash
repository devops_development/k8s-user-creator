https://ahmedelfakharany.com/automate-kubernetes-user-creation-using-the-native-go-client-e2d20dcdc9de
https://github.com/abohmeed/k8susercreator
https://devopstales.github.io/kubernetes/k8s-user-accounts/
https://kubernetes-tutorial.schoolofdevops.com/configuring_authentication_and_authorization/  #VAR 2

https://loft.sh/blog/kubernetes-service-account-what-it-is-and-how-to-use-it/

openssl genrsa -out devopstales.pem
openssl req -new -key devopstales.pem -out devopstales.csr -subj "/CN=devopstales"

#!!??openssl req -new -key devopstales.pem -out ivnilv.csr -subj "/CN=devopstales/O=devops-groupe"
cat <<'EOF'> devopstales-csr.yaml
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: user-request-devopstales
spec:
  groups:
  - system:authenticated
  request: LS0tLS1CRUdJTi...
  usages:
  - digital signature
  - key encipherment
  - client auth
EOF

kubectl create -f devopstales-csr.yaml

kubectl get csr user-request-devopstales -o jsonpath='{.status.certificate}' | base64 -d > devopstales-user.crt



kubectl --kubeconfig ~/.kube/config-devopstales config set-cluster preprod --insecure-skip-tls-verify=true --server=https://192.168.1.164:6443
kubectl --kubeconfig ~/.kube/config-devopstales config set-credentials devopstales --client-certificate=devopstales-user.crt --client-key=devopstales.pem --embed-certs=true
kubectl --kubeconfig ~/.kube/config-devopstales config set-context default --cluster=preprod --user=devopstales
kubectl --kubeconfig ~/.kube/config-devopstales config use-context default

kubectl --kubeconfig ~/.kube/config-devopstales get pods

kubectl --kubeconfig ~/.kube/config-devopstales config set-context --current --namespace test 

kubectl auth can-i create pod --namespace production

k --kubeconfig ~/.kube/config-devopstales create -f https://k8s.io/examples/application/deployment.yaml
k --kubeconfig ~/.kube/config-devopstales delete -f https://k8s.io/examples/application/deployment.yaml
kubectl run nginx --image=nginx



go run ./cmd/k8suser --username user1 --email user1@tenant1.com --cluster multipass --country US --city IL --organization tenant1.com --orgUnit DevOps --province Chicago

go run ./cmd/k8suser --username bob --email "bob@acmecorp.com" --cluster testing.acmecorp.k8s.local

./k8suser --username bob --email "bob@multipass" --cluster multipass


kubectl config set-cluster multipass --server=https://127.0.0.1:6000/proxy/apiserver --insecure-skip-tls-verify=true


kubectl config view -o jsonpath='{"Cluster name\tServer\n"}{range .clusters[*]}{.name}{"\t"}{.cluster.server}{"\n"}{end}'

export CLUSTER_NAME="multipass"
APISERVER=$(kubectl config view -o jsonpath="{.clusters[?(@.name==\"$CLUSTER_NAME\")].cluster.server}")


kubectl apply -f - <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: default-token
  annotations:
    kubernetes.io/service-account.name: default
type: kubernetes.io/service-account-token
EOF


# VAR 2

mkdir -p  ~/.kube/users
cd ~/.kube/users

openssl genrsa -out alex.key 2048
openssl req -new -key alex.key -out alex.csr -subj "/CN=alex/O=alex"

scp cube01:/var/lib/rancher/k3s/server/tls/client-ca.* .


openssl x509 -req -CA client-ca.crt -CAkey client-ca.key -CAcreateserial -days 730 -in alex.csr -out alex.crt


#kubectl config set-credentials alex --client-certificate=/home/juno/.kube/users/alex.crt --client-key=/home/juno/.kube/users/alex.key


#kubectl config set-context alex-kubernetes --cluster=multipass --user=alex --namespace=test

kubectl config use-context multipass

k apply -f roles/readonly-role.yaml
k apply -f roles/readonly-rolebinding.yml

kubectl config use-context alex-kubernetes
k  delete -f https://k8s.io/examples/application/deployment.yaml



kubectl config use-context multipass


kubectl --kubeconfig ~/.kube/config-alex config set-cluster development --insecure-skip-tls-verify=true --server=https://192.168.1.164:6443
kubectl --kubeconfig ~/.kube/config-alex config set-credentials alex --client-certificate=/home/juno/.kube/users/alex.crt --client-key=/home/juno/.kube/users/alex.key --embed-certs=true
kubectl --kubeconfig ~/.kube/config-alex config set-context default --cluster=development --user=alex
kubectl --kubeconfig ~/.kube/config-alex config use-context default


k delete -f roles/readonly-rolebinding.yml


kubectl create deployment nginx1 --image=nginx
./apiscript.bash

kubectl create serviceaccount nginx-serviceaccount

kubectl create rolebinding nginx-sa-readonly \
  --clusterrole=cluster-admin \
  --serviceaccount=default:nginx-serviceaccount \
  --namespace=default


k apply -f nginxdeploy.yaml

 kubectl describe sa nginx-serviceaccount


kubectl create serviceaccount pod-viewer

k apply -f  clusterrole.yaml


kubectl create clusterrolebinding pod-viewer \
  --clusterrole=pod-viewer \
  --serviceaccount=default:pod-viewer


  kubectl exec -it my-pod-with-service-account -- bash

  TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

  curl -H "Authorization: Bearer $TOKEN" https://kubernetes/api/v1/namespaces/default/pods/ --insecure



